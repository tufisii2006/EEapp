package com.fortech.sports.customResponseMessages;

public class CustomResponseMessages {

	public static final String HTTP_Ok = "Success!";
	
	public static final String HTTP_BadRequest = "The request could not be performed!";

}
