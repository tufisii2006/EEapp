package com.fortech.sports.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Used for getting the start and end time of a certain day date
 * 
 * @author radu.tufisi
 *
 */
public class BookingDateUtils {

	/**
	 * Return a Date having the time stamp at 8 in the morning
	 * 
	 * @param date - the string date with the format "yyyy-MM-dd" that will be
	 *             converted to {@link Date} at 8 in the morning
	 * @return {@link Date}
	 * @throws ParseException - if the format is invalid
	 */
	public static Date getTheMorningTimeOfGivenDay(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj = sdf.parse(date);
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateObj);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 00);
		Date startDate = calendar.getTime();
		return startDate;
	}

	/**
	 * Return a Date having the time stamp at 23:59 midnight
	 * 
	 * @param date -he string date with the format "yyyy-MM-dd" that will be
	 *             converted to {@link Date} at 8 in the morning
	 * @return {@link Date}
	 * @throws ParseException - if the format is invalid
	 */
	public static Date getTheEndOfTheGivenDay(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj = sdf.parse(date);
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateObj);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 59);
		Date endDate = calendar.getTime();
		return endDate;
	}

}
