package com.fortech.sports.customExceptions;

public class InvalidProvidedArgument extends Exception{

	private static final long serialVersionUID = -825016939030567713L;

	public InvalidProvidedArgument(String message) {
		super(message); 
	}

}
