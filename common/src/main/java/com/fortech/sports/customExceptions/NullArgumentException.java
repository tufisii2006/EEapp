package com.fortech.sports.customExceptions;

/**
 * Exception used when the argument of method is null
 * @author radu.tufisi
 *
 */
public class NullArgumentException extends Exception {

	private static final long serialVersionUID = 5590335431176749300L;

	public NullArgumentException(String message) {
		super(message); 
	}

}
