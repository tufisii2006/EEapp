package com.fortech.sports.customExceptions;

public class NoSuchEntryException extends Exception {

	private static final long serialVersionUID = 1110150819392177631L;

	public NoSuchEntryException(String message) {
		super(message);
	}
}
