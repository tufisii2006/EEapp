package com.fortech.sports.customExceptions;

/**
 * Exception used by BaseRepository when performing CRUD operations.
 *
 */
public class RepositoryException extends Exception {

	private static final long serialVersionUID = 1L;

	public RepositoryException(String message) {
		super(message);
	}

}
