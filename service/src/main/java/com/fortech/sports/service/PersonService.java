package com.fortech.sports.service;

import java.util.NoSuchElementException;
import java.util.Set;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Person;

/**
 * Handles CRUD operations on {@link Person} entity
 *
 */
public interface PersonService {

	/**
	 * Inserts a new {@link Person} entity in the DataBase.
	 * 
	 * @param person
	 *            - The {@link Person} that should be inserted.
	 * @throws RepositoryException
	 *             - if the DataBase operation could not be performed
	 * @throws NullArgumentException
	 *             - if the provided {@link Person} is null
	 */
	void addPerson(Person person) throws RepositoryException, NullArgumentException;

	/**
	 * Delete an existing {@link Person} entity from the DataBase.
	 * 
	 * @param person
	 *            - The {@link Person} that will be deleted.
	 * @throws RepositoryException
	 *             - if the DataBase operation could not be performed.
	 * @throws NullArgumentException
	 *             - if the provided {@link Person} is null.
	 * @throws NoSuchEntryException
	 *             - if the provided {@link Person} does not exist in the DataBase.
	 */
	void deletePerson(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve an existing {@link Person} entity from the DataBase;
	 * 
	 * @param id
	 *            - The id of the {@link Person}.
	 * @return A {@link Person} entity with the provided id.
	 * @throws RepositoryException
	 *             - if the DataBase operation could not be performed.
	 * @throws NullArgumentException
	 *             - if the provided id is null.
	 */
	Person getOnePersonById(Long id) throws RepositoryException, NullArgumentException,NoSuchEntryException;

	/**
	 * Retrieve all the {@link Person} entities from the DataBase.
	 * 
	 * @return List<{@link Person}>.
	 * @throws RepositoryException
	 *             - if the DataBase operation could not be performed.
	 */
	Set<Person> getAllPersons() throws RepositoryException;

	/**
	 * Retrieve a {@link Person} entity from the DataBase
	 * 
	 * @param email
	 *            - The email of the person to be found
	 * @return {@link Person} entity
	 * @throws NullArgumentException
	 *             - if the provided email is null
	 * @throws RepositoryException
	 *             - if the DataBase operation could not be performed.
	 * @throws NoSuchElementException
	 *             - if no {@link Person} entities exist in the DataBase.
	 */
	Person getOnePersonByEmail(String email) throws NullArgumentException, RepositoryException, NoSuchEntryException;

}
