package com.fortech.sports.service;

import java.util.Date;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.SportType;
import com.fortech.sports.repository.FieldRepository;

@Stateless
public class FieldServiceImpl implements FieldService {

	@EJB
	private FieldRepository fieldRepository;

	@Override
	public void addField(Field field) throws RepositoryException, NullArgumentException {
		if (field == null) {
			throw new NullArgumentException("The provided field is null!");
		}
		fieldRepository.save(field);
	}

	@Override
	public void deleteField(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The provided id is null!");
		}
		Field field = fieldRepository.findById(id);
		if (field == null) {
			throw new NoSuchEntryException("No field having that id was found!");
		}
		fieldRepository.delete(field);
	}

	@Override
	public Field getOneFieldById(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The field ID is null!");
		}
		Field field = fieldRepository.findById(id);
		if (field == null) {
			throw new NoSuchEntryException("No field having that id was found!");
		}
		return field;
	}

	@Override
	public Set<Field> getAllFields() throws RepositoryException {
		Set<Field> allFieldsFromDB = fieldRepository.findAll();
		return allFieldsFromDB;
	}

	@Override
	public Set<Field> getAllFieldsOfGivenSportType(SportType sportType)
			throws RepositoryException, NullArgumentException {
		if (sportType == null) {
			throw new NullArgumentException("The sport type is null!");
		}
		Set<Field> allFieldsFromDB = fieldRepository.findBySportType(sportType);
		return allFieldsFromDB;
	}

	@Override
	public Set<Field> getAllFieldsAvailableBetweenTimeIntervals(Date startTime, Date endTime)
			throws RepositoryException, NullArgumentException {
		if (startTime == null || endTime == null) {
			throw new NullArgumentException("The start time or end time is null!");
		}
		return fieldRepository.findAllAvailableBetweenTimeIntervals(startTime, endTime);
	}

	@Override
	public Set<Field> getAllFieldsByRegion(String address) throws NullArgumentException {
		if (address == null) {
			throw new NullArgumentException("The address is null!");
		}
		return fieldRepository.findAllFieldsByRegion(address);
	}

}
