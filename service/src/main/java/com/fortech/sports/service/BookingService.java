package com.fortech.sports.service;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;

import com.fortech.sports.customExceptions.InvalidProvidedArgument;
import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Booking;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;

/**
 * Handles CRUD operations on {@link Booking} entity
 *
 */
public interface BookingService {

	/**
	 * Inserts a new {@link Booking} entity in the DataBase.
	 * 
	 * @param booking - The {@link Booking} that should be inserted.
	 * @throws RepositoryException     - if the DataBase operation could not be
	 *                                 performed
	 * @throws NullArgumentException   - if the provided {@link Booking} is null
	 * @throws NoSuchEntryException    - if the provided field of owner does not
	 *                                 exists in the DataBase
	 * @throws InvalidProvidedArgument -if the provided StartDate and EndDate
	 *                                 overlap another {@link Booking}
	 */
	void createBooking(Booking booking, Long idPerson, Long idField)
			throws InvalidProvidedArgument, RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Delete an existing {@link Booking} entity from the DataBase.
	 * 
	 * @param id - The id of {@link Booking} that will be deleted.
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException - if the provided {@link Booking} is null.
	 * @throws NoSuchEntryException  - if the provided {@link Booking} does not
	 *                               exist in the DataBase.
	 */
	void deleteBooking(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve an existing {@link Booking} entity from the DataBase;
	 * 
	 * @param id - The id of the {@link Booking}.
	 * @return A {@link Booking} entity with the provided id.
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException - if the provided id is null.
	 * @throws NoSuchEntryException  - if no {@link Booking} having the provided id
	 *                               was found.
	 */
	Booking getOneBookingById(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve all the {@link Booking} entities from the DataBase.
	 * 
	 * @return List<{@link Booking}>.
	 * @throws RepositoryException  - if the DataBase operation could not be
	 *                              performed.
	 * @throws NoSuchEntryException - if no {@link Booking} entities exist in the
	 *                              DataBase.
	 */
	Set<Booking> getAllBooking() throws RepositoryException, NoSuchEntryException;

	/**
	 * Adds a new {@link Person} to an existing {@link Booking}
	 * 
	 * @param idBooking - the id of {@link Booking}
	 * @param person    - the {@link Person} to be inserted
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException - if any of the arguments are null
	 * @throws NoSuchEntryException  - if the {@link Booking} does not exists
	 */
	void addPersonToExistingBooking(Long idBooking, Person person)
			throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Removes a {@link Person} from and existing {@link Booking}
	 * 
	 * @param idBooking - the id of {@link Booking}
	 * @param idPerson  - the id of {@link Person}
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException -if any of the arguments are null
	 * @throws NoSuchEntryException  - if the {@link Person} or the {@link Booking}
	 *                               does not exist in the DataBase
	 */
	void removePersonFromExistingBooking(Long idBooking, Long idPerson)
			throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve a Set of {@link Booking} that are created by a {@link Person}
	 * 
	 * @param idPerson - the id of {@link Person}
	 * @return Set {@link Booking}
	 * @throws RepositoryException-     if the DataBase operation could not be
	 *                                  performed.
	 * @throws NullArgumentException    - if any arguments are null
	 * @throws NoSuchElementException   - if no {@link Person} having that id exists
	 *                                  in the DataBase
	 * @throws EmptyListResultException - if the person has no {@link Booking}
	 */
	Set<Booking> getAllBookingOfGivenPerson(Long idPerson)
			throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve a Set of {@link Booking} that are registered in one day
	 * 
	 * @param fieldId   - the id of {@link Field}
	 * @param startDate - the start date
	 * @param endDate   - the end date
	 * @return Set {@link Booking}
	 * @throws NullArgumentException - if any argument is null
	 */
	Set<Booking> getAllAvailableBookingsSortedByFieldAndStartDateAndEndDate(Long fieldId, Date startDate, Date endDate)
			throws NullArgumentException;

	/**
	 * Retrieve a set of {@link Booking} where a person is participant
	 * 
	 * @param personId - the id of person
	 * @return Set {@link Booking}
	 * @throws NullArgumentException - if the person id is null
	 * @throws NoSuchEntryException  - if the person does not exist in the DataBase
	 */
	Set<Booking> getAllBookingsOfAPersonWhereHeParticipate(Long personId)
			throws NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve a set of {@link Booking} based on the {@link Field} they are booked
	 * 
	 * @param fieldId - the id of {@link Field}
	 * @return Set {@link Booking}
	 * @throws NullArgumentException - if the id is null
	 * @throws RepositoryException   - if the operation could not be performed
	 */
	Set<Booking> getAllBookingsByAField(Long fieldId)
			throws NullArgumentException, RepositoryException, NoSuchEntryException;

	/**
	 * Retrieve a set of {@link Booking} between a period of time
	 * 
	 * @param startDate - the start time
	 * @param endDate   - the end time
	 * @return Set {@link Booking}
	 * @throws NullArgumentException - if any argument is null
	 */
	Set<Booking> getAllBookingsBetweenTimeIntervals(Date startDate, Date endDate) throws NullArgumentException;
}
