package com.fortech.sports.service;

import java.util.NoSuchElementException;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Person;
import com.fortech.sports.repository.PersonRepository;

@Stateless
public class PersonServiceImpl implements PersonService {

	@EJB
	private PersonRepository personRepository;

	@Override
	public void addPerson(Person person) throws RepositoryException, NullArgumentException {
		if (person == null) {
			throw new NullArgumentException("The provided Person is null!");
		}
		personRepository.save(person);
	}

	@Override
	public void deletePerson(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The provided id is null!");
		}
		Person personToBeDeleted = personRepository.findById(id);
		if (personToBeDeleted == null) {
			throw new NoSuchEntryException("The provided Person does not exist!");
		}
		personRepository.delete(personToBeDeleted);
	}

	@Override
	public Person getOnePersonById(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The provided id is null!");
		}
		Person person = personRepository.findById(id);
		if (person == null) {
			throw new NoSuchEntryException("No person having that id was found!");
		}
		return person;
	}

	@Override
	public Set<Person> getAllPersons() throws RepositoryException {
		Set<Person> allPersons = personRepository.findAll();
		return allPersons;
	}

	@Override
	public Person getOnePersonByEmail(String email)
			throws NullArgumentException, RepositoryException, NoSuchEntryException {
		if (StringUtils.isEmpty(email)) {
			throw new NullArgumentException("The provided email is null!");
		}
		Person person = personRepository.findByEmail(email);
		if (person == null) {
			throw new NoSuchElementException("No person having that email was found!");
		}
		return person;
	}

}
