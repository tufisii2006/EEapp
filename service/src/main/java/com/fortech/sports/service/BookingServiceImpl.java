package com.fortech.sports.service;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Predicate;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.fortech.sports.customExceptions.InvalidProvidedArgument;
import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Booking;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;
import com.fortech.sports.repository.BookingRepository;
import com.fortech.sports.repository.FieldRepository;
import com.fortech.sports.repository.PersonRepository;

@Stateless
public class BookingServiceImpl implements BookingService {

	@EJB
	private BookingRepository bookingRepository;

	@EJB
	private PersonRepository personRepository;

	@EJB
	private FieldRepository fieldRepository;

	@Override
	public void createBooking(Booking booking, Long idPerson, Long idField)
			throws InvalidProvidedArgument, RepositoryException, NullArgumentException, NoSuchEntryException {
		if (booking == null || idPerson == null) {
			throw new NullArgumentException("The provided booking or person id is null!");
		}
		Person bookingOwner = personRepository.findById(idPerson);
		Field field = fieldRepository.findById(idField);
		if (bookingOwner == null) {
			throw new NoSuchEntryException("No person having the provided id was found!");
		}
		if (field == null) {
			throw new NoSuchEntryException("No field having the provided id was found!");
		}
		booking.setCreatedBy(bookingOwner);
		booking.setField(field);
		booking.getAtendees().add(bookingOwner);

		Set<Booking> allBookingsSpecificDate = bookingRepository.findByFieldIdAndStartDateAndEndDate(idField,
				booking.getStartTime(), booking.getEndTime());
		if (allBookingsSpecificDate.size() > 0) {
			throw new InvalidProvidedArgument("Booking overlap exception! Please check the booking start/end times!");
		}
		bookingRepository.save(booking);

	}

	@Override
	public void deleteBooking(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The provided id is null!");
		}
		Booking booking = bookingRepository.findById(id);
		if (booking == null) {
			throw new NoSuchEntryException("No booking having that id was found!");
		}
		bookingRepository.delete(booking);
	}

	@Override
	public Booking getOneBookingById(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (id == null) {
			throw new NullArgumentException("The provided id is null!");
		}
		Booking booking = bookingRepository.findById(id);
		if (booking == null) {
			throw new NoSuchEntryException("No booking having that id was found!");
		}
		return booking;
	}

	@Override
	public Set<Booking> getAllBooking() throws RepositoryException {
		Set<Booking> allBookingsFromDataBase = bookingRepository.findAll();
		return allBookingsFromDataBase;
	}

	@Override
	public void addPersonToExistingBooking(Long idBooking, Person person)
			throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (idBooking == null || person == null) {
			throw new NullArgumentException("The provided booking id or person is null!");
		}
		Booking booking = bookingRepository.findById(idBooking);
		if (booking == null) {
			throw new NoSuchEntryException("No booking having that id was found!");
		}
		Set<Person> allParticipantsOfBooking = booking.getAtendees();
		if (allParticipantsOfBooking.size() == booking.getField().getCapacity()) {
			throw new RepositoryException("No more persons can be added to this booking!Field capacity is full!");
		}
		allParticipantsOfBooking.add(personRepository.findById(person.getId()));
		bookingRepository.save(booking);
	}

	@Override
	public void removePersonFromExistingBooking(Long idBooking, Long idPerson)
			throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (idBooking == null || idPerson == null) {
			throw new NullArgumentException("The provided booking id or person id is null!");
		}
		Booking booking = bookingRepository.findById(idBooking);
		Person person = personRepository.findById(idPerson);
		if (booking == null || person == null) {
			throw new NoSuchEntryException("No booking or person having that id was found!");
		}
		Set<Person> allParticipantsOfBooking = booking.getAtendees();

		Predicate<Person> personPredicate = p -> p.getId() == idPerson;

		if (allParticipantsOfBooking.removeIf(personPredicate) == false) {
			throw new NoSuchElementException("The provided person is not present in the current booking!");
		}
		bookingRepository.save(booking);
	}

	@Override
	public Set<Booking> getAllBookingOfGivenPerson(Long idPerson)
			throws RepositoryException, NullArgumentException, NoSuchEntryException {
		if (idPerson == null) {
			throw new NullArgumentException("The provided person id is null!");
		}
		Person person = personRepository.findById(idPerson);
		if (person == null) {
			throw new NoSuchEntryException("No person having that id was found!");
		}
		Set<Booking> bookings = bookingRepository.findByPerson(person);
		return bookings;
	}

	@Override
	public Set<Booking> getAllAvailableBookingsSortedByFieldAndStartDateAndEndDate(Long fieldId, Date startDate,
			Date endDate) throws NullArgumentException {
		Set<Booking> bookings = bookingRepository.findByFieldIdAndStartDateAndEndDate(fieldId, startDate, endDate);
		return bookings;
	}

	@Override
	public Set<Booking> getAllBookingsOfAPersonWhereHeParticipate(Long personId)
			throws NullArgumentException, NoSuchEntryException {
		if (personId == null) {
			throw new NullArgumentException("The provided  id is null!");
		}
		Set<Booking> bookings = bookingRepository.findAllBookingsOfAPerson(personId);
		return bookings;
	}

	@Override
	public Set<Booking> getAllBookingsByAField(Long fieldId) throws NullArgumentException, RepositoryException,NoSuchEntryException {
		if (fieldId == null) {
			throw new NullArgumentException("The provided  id is null!");
		}
		Field field = fieldRepository.findById(fieldId);
		if(field==null) {
			throw new NoSuchEntryException("No field having that id was found!");
		}
		return bookingRepository.findByField(field);
	}

	@Override
	public Set<Booking> getAllBookingsBetweenTimeIntervals(Date startDate, Date endDate) throws NullArgumentException {
		if (startDate == null || endDate == null) {
			throw new NullArgumentException("The provided  id is null!");
		}
		return bookingRepository.findByStartDateAndEndDate(startDate, endDate);
	}

}
