package com.fortech.sports.service;

import java.util.Date;
import java.util.Set;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.SportType;

/**
 * Handles CRUD operations on {@link Field} entity
 * 
 *
 */
public interface FieldService {

	/**
	 * Inserts a new {@link Field} entity in the DataBase.
	 * 
	 * @param field - The {@link Field} that should be inserted.
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed
	 * @throws NullArgumentException - if the provided {@link Field} is null
	 */
	void addField(Field field) throws RepositoryException, NullArgumentException;

	/**
	 * Delete an existing {@link Field} entity from the DataBase.
	 * 
	 * @param id - The id of {@link Field} that will be deleted.
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException - if the provided {@link Field} is null.
	 * @throws NoSuchEntryException  - if the provided {@link Field} does not exist
	 *                               in the DataBase.
	 */
	void deleteField(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve an existing {@link Field} entity from the DataBase;
	 * 
	 * @param id - The id of the {@link Field}.
	 * @return A {@link Field} entity with the provided id.
	 * @throws RepositoryException   - if the DataBase operation could not be
	 *                               performed.
	 * @throws NullArgumentException - if the provided id is null.
	 * @throws NoSuchEntryException  - if no {@link Field} having the provided id
	 *                               was found.
	 */
	Field getOneFieldById(Long id) throws RepositoryException, NullArgumentException, NoSuchEntryException;

	/**
	 * Retrieve all the {@link Field} entities from the DataBase.
	 * 
	 * @return Set<{@link Field}>.
	 * @throws RepositoryException - if the DataBase operation could not be
	 *                             performed.
	 */
	Set<Field> getAllFields() throws RepositoryException;

	/**
	 * Retrieve set of {@link Field} entities from the DataBase based on
	 * {@link Field} {@link SportType}
	 * 
	 * @param sportType - the {@link SportType} to be used to retrieve the set
	 * @return Set {@link Field}
	 * @throws RepositoryException - if the DataBase operation could not be
	 *                             performed.
	 */
	Set<Field> getAllFieldsOfGivenSportType(SportType sportType) throws RepositoryException, NullArgumentException;

	/**
	 * Retrieve set of {@link Field} entities from the DataBase based on a time
	 * interval
	 * 
	 * @param startTime - the start time
	 * @param endTime   - the end time
	 * @return Set {@link Field}
	 * @throws RepositoryException   - if the operation could not be performed
	 * @throws NullArgumentException -if any argument is null
	 */
	Set<Field> getAllFieldsAvailableBetweenTimeIntervals(Date startTime, Date endTime)
			throws RepositoryException, NullArgumentException;

	/**
	 * 	 * Retrieve set of {@link Field} entities from the DataBase based on their region
	 * @param address - the region to search
	 * @return Set {@link Field}
	 * @throws NullArgumentException - if the address is null
	 */
	Set<Field> getAllFieldsByRegion(String address) throws NullArgumentException;

}
