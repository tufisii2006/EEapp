package com.fortech.sports.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "booking")
@AttributeOverride(name = "id", column = @Column(name = "idBooking"))
public class Booking extends AbstractEntity<Long> {

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Booking start time can not be null!")
	private Date startTime;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Booking end time can not be null!")
	private Date endTime;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "owner_id")
	private Person createdBy;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "person_booking", joinColumns = @JoinColumn(name = "booking_id", referencedColumnName = "idBooking"), inverseJoinColumns = @JoinColumn(name = "person_id", referencedColumnName = "idPerson"))
	private Set<Person> atendees = new HashSet<Person>();

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "field_id")
	private Field field;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Person getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Person createdBy) {
		this.createdBy = createdBy;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Set<Person> getAtendees() {
		return atendees;
	}

	public void setAtendees(Set<Person> atendees) {
		this.atendees = atendees;
	}

}
