package com.fortech.sports.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "person")
@AttributeOverride(name = "id", column = @Column(name = "idPerson"))
public class Person extends AbstractEntity<Long> {

	@Column
	@NotNull(message = "Person name can not be null!")
	@Size(min = 3, message = "Person name should contain at least 3 characters!")
	private String name;

	@Column
	@NotNull(message = "Person email can not be null!")
	@Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Not a valid email address!")
	private String email;

	@Column
	@NotNull(message = "Phone number can not be null!")
	@Size(min = 10, max = 10, message = "Phone number should contain only 10 digits!")
	private String phoneNumber;

	public Person() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", email=" + email + ", phoneNumber=" + phoneNumber + "]";
	}

}
