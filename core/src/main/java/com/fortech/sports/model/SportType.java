package com.fortech.sports.model;

public enum SportType {

	TENNIS("Tennis"), FOOTBALL("Football"), BASKETBALL("Basketball"), RUGBY("Rugby"), RUNNING("Running"),BADMINTON(
			"Badminton");

	private final String value;

	SportType(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link SportType} for the given String.
	 * 
	 * @param sportTypeTypeString
	 * 
	 * @return associated {@link SportType} for the given String
	 */
	public static SportType fromString(final String sportTypeTypeString) {
		for (final SportType sportType : SportType.values()) {
			if (sportType.value().equalsIgnoreCase(sportTypeTypeString)) {
				return sportType;
			}
		}
		return null;
	}
}
