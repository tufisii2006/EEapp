package com.fortech.sports.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "field")
@AttributeOverride(name = "id", column = @Column(name = "idField"))
@XmlRootElement
public class Field extends AbstractEntity<Long> {

	@Column
	@NotNull(message = "Field name can not be null!")
	@Size(min = 3, message = "Field name should contain at least 3 characters!")
	private String name;

	@Column
	@NotNull(message = "Field address can not be null!")
	private String address;

	@Column
	@NotNull(message = "Field capacity can not be null!")
	@Min(value = 0L, message = "The capacity value must be a positive value!")
	private Integer capacity;

	@Column
	@NotNull(message = "Field price can not be null!")
	@Min(value = 0L, message = "The price value must be a positive value!")
	private Double price;

	@Column
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Field type can not be null!")
	private SportType sportType;

	@Column
	private String details;

	public Field() {

	}

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlElement
	public SportType getSportType() {
		return sportType;
	}

	public void setSportType(SportType sportType) {
		this.sportType = sportType;
	}

	@XmlElement
	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	@XmlElement
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@XmlElement
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", address=" + address + ", capacity=" + capacity + ", price=" + price
				+ ", sportType=" + sportType + ", details=" + details + "]"+"id:"+this.getId();
	}
	
	

}
