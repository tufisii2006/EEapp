package com.fortech.sports.repository;

import java.util.Date;
import java.util.Set;

import com.fortech.sports.abstractRepository.BaseRepository;
import com.fortech.sports.model.Booking;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;

/**
 * Performs DataBase operation methods for {@link Booking} Entity
 *
 */
public interface BookingRepository extends BaseRepository<Booking, Long> {

	/**
	 * Retrieve a set of all {@link Booking} between 2 dates
	 * 
	 * @param startDate - The first date to search
	 * @param endDate   - The second date to search
	 * @return Set< {@link Booking} >
	 */
	Set<Booking> findByStartDateAndEndDate(Date startDate, Date endDate);

	/**
	 * Retrieve a set of {@link Booking} of a specific person
	 * 
	 * @param person - The {@link Person} that will be used to retrieve its bookings
	 * @return Set {@link Booking}
	 */
	Set<Booking> findByPerson(Person person);

	/**
	 * Retrieve a set of {@link Booking} of a certain field
	 * 
	 * @param field - the {@link Field} that will be used to retrieve the list
	 * @return Set {@link Booking}
	 */
	Set<Booking> findByField(Field field);

	/**
	 * Retrieve a set of {@link Booking} based on field on a certain day
	 * 
	 * @param fieldId   - the {@link Field} that will be used to retrieve the list
	 * @param startDate - the start date
	 * @param endDate   - the end date
	 * @return Set {@link Booking}
	 */
	Set<Booking> findByFieldIdAndStartDateAndEndDate(Long fieldId, Date startDate, Date endDate);

	/**
	 * Retrieve a set of {@link Booking} where a {@link Person} is participant
	 * @param personId - the id of the {@link Person}
	 * @return Set {@link Booking}
	 */
	 Set<Booking> findAllBookingsOfAPerson(Long personId);

}
