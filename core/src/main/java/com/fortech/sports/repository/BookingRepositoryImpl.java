package com.fortech.sports.repository;

import java.util.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.fortech.sports.abstractRepository.BaseRepositoryBean;
import com.fortech.sports.model.Booking;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;

@Stateless
public class BookingRepositoryImpl extends BaseRepositoryBean<Booking, Long> implements BookingRepository {

	@PersistenceContext(name = "Sports")
	private EntityManager entityManager;

	public BookingRepositoryImpl() {
		super(Booking.class);
	}

	@Override
	public Set<Booking> findByStartDateAndEndDate(Date startDate, Date endDate) {
		TypedQuery<Booking> findByStartDateAndEndDate = entityManager
				.createNamedQuery("Booking.findByStartDateAndEndDate", Booking.class)
				.setParameter("startDate", startDate, TemporalType.TIMESTAMP)
				.setParameter("endDate", endDate, TemporalType.TIMESTAMP);
		Set<Booking> allBookingsByInputDate = new HashSet<Booking>(findByStartDateAndEndDate.getResultList());
		return allBookingsByInputDate;
	}

	@Override
	public Set<Booking> findByPerson(Person person) {
		TypedQuery<Booking> findByPerson = entityManager.createNamedQuery("Booking.findByPerson", Booking.class)
				.setParameter("person", person);
		Set<Booking> allBookingsByInputDate = new HashSet<Booking>(findByPerson.getResultList());

		return allBookingsByInputDate;
	}

	@Override
	public Set<Booking> findByField(Field field) {
		TypedQuery<Booking> findByField = entityManager.createNamedQuery("Booking.findByField", Booking.class)
				.setParameter("field", field);
		Set<Booking> allBookingsFromFieldType = new HashSet<Booking>(findByField.getResultList());
		return allBookingsFromFieldType;
	}

	@Override
	public Set<Booking> findByFieldIdAndStartDateAndEndDate(Long fieldId, Date startDate, Date endDate) {
		Field field = entityManager.find(Field.class, fieldId);
		TypedQuery<Booking> findByFieldIdAndStartDateAndEndDate = entityManager
				.createNamedQuery("Booking.findByFieldIdAndStartDateAndEndDate", Booking.class)
				.setParameter("startDate", startDate, TemporalType.TIMESTAMP)
				.setParameter("endDate", endDate, TemporalType.TIMESTAMP).setParameter("fieldId", field);
		Set<Booking> allBookingsByInputDate = new HashSet<Booking>(findByFieldIdAndStartDateAndEndDate.getResultList());
		return allBookingsByInputDate;
	}

	@Override
	public Set<Booking> findAllBookingsOfAPerson(Long personId) {
		TypedQuery<Booking> findAllBookingsOfAPerson = entityManager
				.createNamedQuery("Booking.findAllBookingsOfAPerson", Booking.class).setParameter("id", personId);
		Set<Booking> persons = new HashSet<Booking>(findAllBookingsOfAPerson.getResultList());
		return persons;
	}

}
