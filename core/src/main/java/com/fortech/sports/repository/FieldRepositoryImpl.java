package com.fortech.sports.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.fortech.sports.abstractRepository.BaseRepositoryBean;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.SportType;

@Stateless
public class FieldRepositoryImpl extends BaseRepositoryBean<Field, Long> implements FieldRepository {

	@PersistenceContext(name = "Sports")
	private EntityManager entityManager;

	public FieldRepositoryImpl() {
		super(Field.class);
	}

	@Override
	public Set<Field> findBySportType(SportType sportType)  {
		TypedQuery<Field> findBySportType = entityManager.createNamedQuery("Field.findBySportType", Field.class)
				.setParameter("sportType", sportType);
		Set<Field> allFieldsOfGivenType = new HashSet<Field>(findBySportType.getResultList());
		return allFieldsOfGivenType;
	}

	@Override
	public Set<Field> findAllAvailableBetweenTimeIntervals(Date startTime, Date endTime) {
		TypedQuery<Field> findAllAvailableBetweenTimeIntervals = entityManager
				.createNamedQuery("Field.findAllFreeFieldsInTimePeriod", Field.class)
				.setParameter("startDate", startTime, TemporalType.TIMESTAMP)
				.setParameter("endDate", endTime, TemporalType.TIMESTAMP);
		Set<Field> allFieldsOfGivenType = new HashSet<Field>(findAllAvailableBetweenTimeIntervals.getResultList());
		return allFieldsOfGivenType;
	}

	@Override
	public Set<Field> findAllFieldsByRegion(String address) {
		TypedQuery<Field> findAllFieldsByRegion = entityManager
				.createNamedQuery("Field.findAllFieldsInRegion", Field.class).setParameter("address", address);
		Set<Field> allFieldsOfGivenType = new HashSet<Field>(findAllFieldsByRegion.getResultList());
		return allFieldsOfGivenType;
	}

}
