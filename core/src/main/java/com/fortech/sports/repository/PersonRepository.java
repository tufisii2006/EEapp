package com.fortech.sports.repository;

import com.fortech.sports.abstractRepository.BaseRepository;
import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.model.Person;

/**
 * Performs DataBase operation methods for {@link Person} Entity
 *
 */

public interface PersonRepository extends BaseRepository<Person, Long> {

	/**
	 * Retrieve a {@link Person} entity from the DataBase using its email
	 * 
	 * @param email - The email of the {@link Person}
	 * @return {@link Person}
	 * @throws NullArgumentException - if the email is null
	 * @throws                       {@link NoSuchEntryException} - if the
	 *                               {@link Person} having that email does not exist
	 *                               in the DataBase
	 */
	 Person findByEmail(String email) throws NullArgumentException, NoSuchEntryException;

	
}
