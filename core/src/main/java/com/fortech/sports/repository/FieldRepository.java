package com.fortech.sports.repository;

import java.util.Date;
import java.util.Set;

import com.fortech.sports.abstractRepository.BaseRepository;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.SportType;

/**
 * Performs DataBase operation methods for {@link Field} Entity
 *
 */
public interface FieldRepository extends BaseRepository<Field, Long> {

	/**
	 * Retrieve all {@link Field} entities having the provided sport type
	 * 
	 * @param sportType
	 *            - The sport type to search
	 * @return Set<{@link Field}>
	 */
	Set<Field> findBySportType(SportType sportType);
	
	/**
	 * Retrieve a set of {@link Field} entities that are not booked in a period of time
	 * 
	 * @param startTime - the start time  
	 * @param endTime  - the end time
	 * @return Set<{@link Field}>
	 */
	Set<Field> findAllAvailableBetweenTimeIntervals(Date startTime,Date endTime);
	
	/**
	 * Retrieve a set of {@link Field} entities based on their region
	 * 
	 * @param address - the region to search
	 * @return Set<{@link Field}>
	 */
	Set<Field> findAllFieldsByRegion(String address);
}
