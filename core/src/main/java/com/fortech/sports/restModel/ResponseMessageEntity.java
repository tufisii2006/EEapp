package com.fortech.sports.restModel;

import javax.ws.rs.core.Response.Status;

public class ResponseMessageEntity {

	private String message;

	private Status httpStatus;

	public Status getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(Status httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ResponseMessageEntity(String Message, Status httpStatus) {
		this.message = Message;
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
