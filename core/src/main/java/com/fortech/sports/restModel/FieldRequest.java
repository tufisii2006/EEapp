package com.fortech.sports.restModel;

import com.fortech.sports.model.SportType;

public class FieldRequest {

	private Long id;

	private String name;

	private String address;

	private Integer capacity;

	private Double price;

	private SportType sportType;

	private String details;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public SportType getSportType() {
		return sportType;
	}

	public void setSportType(SportType sportType) {
		this.sportType = sportType;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "FieldRequest [id=" + id + ", name=" + name + ", address=" + address + ", capacity=" + capacity
				+ ", price=" + price + ", sportType=" + sportType + ", details=" + details + "]";
	}
	
	

}
