package com.fortech.sports.restModel;

import java.util.Date;
import java.util.Set;


import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;

public class BookingResponse{

	public BookingResponse() {

	}

	private Date startTime;

	private Date endTime;

	private Person createdBy;

	private Set<Person> atendees;

	private Field field;

	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Person getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Person createdBy) {
		this.createdBy = createdBy;
	}


	public Set<Person> getAtendees() {
		return atendees;
	}

	public void setAtendees(Set<Person> atendees) {
		this.atendees = atendees;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

}
