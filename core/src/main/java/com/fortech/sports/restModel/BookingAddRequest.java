package com.fortech.sports.restModel;

import java.util.Date;
import java.util.Set;

public class BookingAddRequest {

	private Long id;

	private PersonRequest createdBy;

	private Set<PersonRequest> atendees;

	private FieldRequest field;

	private Date startTime;

	private Date endTime;

	public PersonRequest getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(PersonRequest createdBy) {
		this.createdBy = createdBy;
	}

	public Set<PersonRequest> getAtendees() {
		return atendees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAtendees(Set<PersonRequest> atendees) {
		this.atendees = atendees;
	}

	public FieldRequest getField() {
		return field;
	}

	public void setField(FieldRequest field) {
		this.field = field;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "BookingAddRequest [id=" + id + ", createdBy=" + createdBy + ", atendees=" + atendees + ", field="
				+ field + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}

}
