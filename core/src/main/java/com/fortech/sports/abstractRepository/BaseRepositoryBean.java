package com.fortech.sports.abstractRepository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

import com.fortech.sports.customExceptions.RepositoryException;

public class BaseRepositoryBean<T, I> implements BaseRepository<T, I> {

	@PersistenceContext(unitName = "Sports")
	private EntityManager entityManager;

	private Class<T> classType;

	public BaseRepositoryBean(Class<T> classType) {
		this.classType = classType;
	}

	@Override
	public Set<T> findAll() throws RepositoryException {
		try {
			final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

			final CriteriaQuery<T> cq = cb.createQuery(classType);
			final Root<T> rootEntry = cq.from(classType);
			final CriteriaQuery<T> all = cq.select(rootEntry);

			return new HashSet<T>(entityManager.createQuery(all).getResultList());
		} catch (IllegalArgumentException | PersistenceException ex) {
			throw new RepositoryException("Find all operation failed! " + ex.getMessage());
		}
	}

	@Override
	public T save(T item) throws RepositoryException {
		try {
			entityManager.persist(item);
			return item;
		} catch (IllegalArgumentException | PersistenceException ex) {
			throw new RepositoryException("Save operation failed!");
		} catch (ConstraintViolationException ex) {
			throw new RepositoryException("Invalid data.Please check again the input values!" + ex.getMessage());
		}
	}

	@Override
	public void delete(T item) throws RepositoryException {
		try {
			entityManager.remove(item);
		} catch (IllegalArgumentException | PersistenceException ex) {
			throw new RepositoryException("Delete operation failed! " + ex.getMessage());
		}
	}

	@Override
	public T findById(I id) throws RepositoryException {
		try {
			return entityManager.find(classType, id);
		} catch (IllegalArgumentException | PersistenceException e) {
			throw new RepositoryException("Get by id operation failed! " + e.getMessage());
		}
	}

	public T merge(T item) throws RepositoryException {
		try {
			item = entityManager.merge(item);
			return item;
		} catch (IllegalArgumentException | PersistenceException ex) {
			throw new RepositoryException("Merge operation failed! " + ex.getMessage());
		}
	}

}
