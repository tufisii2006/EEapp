package com.fortech.sports.abstractRepository;

import java.util.Set;

import com.fortech.sports.customExceptions.RepositoryException;

/**
 * Interface that provides basic CRUD operations for all entities.
 * 
 */
public interface BaseRepository<T, I> {
	
	/**
	 * Returns all entries of a type from DataBase
	 * 
	 * @return List of entity type
	 * @throws RepositoryException
	 *             - if the request to DataBase could not be performed
	 */
	Set<T> findAll() throws RepositoryException;

	/**
	 * Return a entry from DataBase of a specific types
	 * 
	 * @param id
	 *            - The id of the specific
	 * @return DataBase entry of a specific type
	 * @throws RepositoryException
	 *             - if the request to DataBase could not be performed
	 */
	T findById(I id) throws RepositoryException;

	/**
	 * Persists a entity to DataBase
	 * 
	 * @param item
	 *            - The object that should be inserted in the DataBase
	 * @return Single object of specific type
	 * @throws RepositoryException
	 *             - if the request to DataBase could not be performed
	 */
	T save(T item) throws RepositoryException;

	/**
	 * Removes a DataBase entry of a specific type
	 * 
	 * @param item
	 *            - The entry that should be deleted
	 * @throws RepositoryException
	 *             - if the request to DataBase could not be performed
	 */
	void delete(T item) throws RepositoryException;

	/**
	 * Updates a DataBase entry of a specific type
	 * 
	 * @param item-
	 *            The entry that should be updated
	 * @return -An object of a specific type
	 * @throws RepositoryException
	 */
	T merge(T item) throws RepositoryException;
}
